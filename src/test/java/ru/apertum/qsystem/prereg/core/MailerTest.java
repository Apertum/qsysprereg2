package ru.apertum.qsystem.prereg.core;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import ru.apertum.qsystem.prereg.Client;
import ru.apertum.qsystem.server.model.QService;

import java.lang.reflect.Field;

import static org.testng.Assert.*;

public class MailerTest {

    @BeforeMethod
    public void setUp() {
    }

    @Test
    @Ignore
    public void testSendReporterMail() throws Exception {
        final Client client = new Client();
        client.setEmail("gmail@gmail.com");
        client.setName("Шура");
        client.setSourname("Каретный");
        client.setPhone("02");
        client.setInputData("Что-то ввели.");
        final QService service = new QService();
        service.setName("тепловые сети!");
        client.setService(service);

        EmailCnf emailCnf = new EmailCnf();
        emailCnf.setEnable(true);
        emailCnf.setFromTitle("Письмо от апертума через яшу");
        emailCnf.setCharset("UTF-8");
        emailCnf.setSubject("Алматинские тепловые сети!");
        emailCnf.setContent("config/email.html");

        Mailer mailer = new Mailer();
        final Field field = Mailer.class.getDeclaredField("emailCnf");
        field.setAccessible(true);
        field.set(mailer, emailCnf);
        mailer.sendReporterMail(client);
    }
}