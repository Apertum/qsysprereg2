package ru.apertum.qsystem.prereg.core;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.*;

public class MultilingualTest {

    Multilingual lng;

    @BeforeMethod
    public void setUp() {
        lng = new Multilingual();
    }

    @Test
    public void test1() {
        Multilingual.Lng ru = lng.init("it", null);
        assertEquals(ru.getLocale(), Locale.ITALY);
    }

    @Test
    public void test2() {
        Multilingual.Lng ru = lng.init("it", "HZ");
        assertEquals(ru.getLocale(), Locale.ITALY);
    }

    @Test
    public void test3() {
        Multilingual.Lng ru = lng.init("it", "IT");
        assertEquals(ru.getLocale(), Locale.ITALY);
    }

    @Test
    public void test4() {
        Multilingual.Lng ru = lng.init("hz", "IT");
        assertEquals(ru.getLocale().getLanguage(), Locale.ENGLISH.getLanguage());
    }
}