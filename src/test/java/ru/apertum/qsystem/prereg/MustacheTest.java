package ru.apertum.qsystem.prereg;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class MustacheTest {

    private MustacheFactory mustacheFactory;
    private Map<String, Object> map;

    @BeforeTest
    public void init() {
        System.out.println("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV");
    }

    @AfterTest
    public void done() {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
    }

    @BeforeMethod
    public void setUp() {
        mustacheFactory = new DefaultMustacheFactory();
        map = new HashMap<>();
    }

    @Test
    public void testFromResource() {
        map.put("key1", "value1");
        final Mustache mustache = mustacheFactory.compile("mustache1.txt");
        final StringWriter writer = new StringWriter();
        Writer writer2 = mustache.execute(writer, map);
        assertEquals(writer, writer2);
        final String str = writer2.toString();
        assertTrue(str.contains(map.get("key1").toString()));
        assertTrue(writer.getBuffer().toString().contains(map.get("key1").toString()));
    }

    @Test
    public void testFromFile() {
        map.put("key1", "value1");
        final Mustache mustache = mustacheFactory.compile("./src/test/resources/mustache1.txt");
        final StringWriter writer = new StringWriter();
        Writer writer2 = mustache.execute(writer, map);
        assertEquals(writer, writer2);
        final String str = writer2.toString();
        assertTrue(str.contains(map.get("key1").toString()));
        assertTrue(writer.getBuffer().toString().contains(map.get("key1").toString()));
    }

    @Test
    public void testFromString() throws MalformedURLException {
        map.put("key1", "value1");
        URL url = new URL("http://yandex.ru:1234/url");
        map.put("url", url);
        StringReader reader = new StringReader("key1={{key1}} host={{url.host}} port={{url.port}}");
        final Mustache mustache = mustacheFactory.compile(reader, "test");
        final String str = mustache.execute(new StringWriter(), map).toString();
        assertTrue(str.contains(map.get("key1").toString()));
        assertEquals(str, "key1=value1 host=yandex.ru port=1234");
    }

    static class Mult {
        public int mult(int m) {
            return m * m;
        }

        public int mult() {
            return 2 * 2;
        }

        public Integer mult2() {
            return null;
        }

        public String getStr() {
            return "s-t-r";
        }
    }

    @Test
    public void testInvokeMeths() {
        map.put("key1", "value1");
        map.put("mult", new Mult());
        StringReader reader = new StringReader("key1={{key1}} 2*2={{mult.mult}}; 3*3 is null{{mult.mult2}}; str={{mult.str}}");
        final Mustache mustache = mustacheFactory.compile(reader, "test");
        final String str = mustache.execute(new StringWriter(), map).toString();
        assertTrue(str.contains(map.get("key1").toString()));
        assertEquals(str, "key1=value1 2*2=4; 3*3 is null; str=s-t-r");
    }

    @Test
    public void testIf() {
        map.put("key1", "value1");
        map.put("key3", true);
        map.put("key4", false);
        StringReader reader = new StringReader("{{#key1}}key1 present={{key1}};" +
                "{{^key2}} key2 not present; {{/key2}}" +
                "{{/key1}}" +
                "{{#key3}}key3=true; {{/key3}}" +
                "{{^key4}}key4=false;{{/key4}}");
        final Mustache mustache = mustacheFactory.compile(reader, "test");
        final String str = mustache.execute(new StringWriter(), map).toString();
        assertEquals(str, "key1 present=value1; key2 not present; key3=true; key4=false;");
    }
}