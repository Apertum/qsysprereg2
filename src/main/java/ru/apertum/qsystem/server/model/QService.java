/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.server.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.common.model.QCustomer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Модель данных для функционирования очереди. Включает в себя: - структуру хранения - методы доступа - методы манипулирования - логирование итераций Главный
 * класс модели данных. Содержит объекты всех кастомеров в очереди к этой услуге. Имеет все необходимые методы для манипулирования кастомерами в пределах одной
 * очереди
 *
 * @author Evgeniy Egorov
 */
@Entity
@Table(name = "services")
public class QService extends DefaultMutableTreeNode implements ITreeIdGetter, Transferable {

    /**
     * Набор статусов услуги, при которых услуга участвует в работе очереди, т.е. к ней могут стоять люди. Это не заглушка и не неактивная.
     */
    public static final Set<Integer> STATUS_FOR_USING = new HashSet<>(Arrays.asList(-1, 1, 2, 4, 5));//NOSONAR
    /**
     * Набор статусов услуги, при которых услуга НЕ участвует в работе очереди, т.е. к ней НЕ могут стоять люди. Это заглушка или неактивная.
     */
    public static final Set<Integer> STATUS_FOR_STUB = new HashSet<>(Arrays.asList(0, 3));//NOSONAR
    /**
     * data flavor used to get back a DnDNode from data transfer.
     */
    public static final DataFlavor DND_NODE_FLAVOR = new DataFlavor(QService.class, "Drag and drop Node");
    /**
     * list of all flavors that this DnDNode can be transfered as.
     */
    protected static DataFlavor[] flavors = {QService.DND_NODE_FLAVOR};
    /**
     * последний номер, выданный последнему кастомеру при номерировании клиентов общем рядом для всех услуг. Ограничение самого минимально возможного номера
     * клиента при сквозном нумерировании происходит при определении параметров нумерации.
     */
    @Transient
    private static volatile int lastStNumber = Integer.MIN_VALUE;
    /**
     * множество кастомеров, вставших в очередь к этой услуге.
     */
    @Transient
    private final PriorityQueue<QCustomer> customers = new PriorityQueue<>();
    @Transient
    //@Expose
    //@SerializedName("clients")
    private final LinkedBlockingDeque<QCustomer> clients = new LinkedBlockingDeque<>(customers);
    @Transient
    @Expose
    @SerializedName("inner_services")
    private final LinkedList<QService> childrenOfService = new LinkedList<>();
    @Id
    @Column(name = "id")
    //@GeneratedValue(strategy = GenerationType.AUTO) авто нельзя, т.к. id нужны для формирования дерева.
    @Expose
    @SerializedName("id")
    private Long id = new Date().getTime();
    /**
     * признак удаления с проставленим даты.
     */
    @Column(name = "deleted")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date deleted;
    /**
     * Состояние услуги. 1 - доступна, 0 - недоступна, -1 - невидима, 2 - только для предвариловки, 3 - заглушка, 4 - не для предвариловки, 5 - рулон.
     */
    @Column(name = "status")
    @Expose
    @SerializedName("status")
    private Integer status;
    /**
     * Пунктов регистрации может быть много. Наборы кнопок на разных киосках могут быть разные. Указание для какого пункта регистрации услуга, 0-для всех, х-для
     * киоска х.
     */
    @Column(name = "point")
    @Expose
    @SerializedName("point")
    private Integer point = 0;
    /**
     * Норматив. Среднее время оказания этой услуги. Зачем надо? Не знаю. Пока для маршрутизации при медосмотре. Может потом тоже применем.
     */
    @Column(name = "duration")
    @Expose
    @SerializedName("duration")
    private Integer duration = 1;
    /**
     * Время обязательного ожидания посетителя.
     */
    @Column(name = "expectation")
    @Expose
    @SerializedName("exp")
    private Integer expectation = 0;
    /**
     * шаблон звукового приглашения. null или 0... - использовать родительский. Далее что играем а что нет.
     */
    @Column(name = "sound_template")
    @Expose
    @SerializedName("sound_template")
    private String soundTemplate;
    @Column(name = "advance_limit")
    @Expose
    @SerializedName("advance_limit")
    private Integer advanceLimit = 1;
    @Column(name = "day_limit")
    @Expose
    @SerializedName("day_limit")
    private Integer dayLimit = 0;
    @Column(name = "person_day_limit")
    @Expose
    @SerializedName("person_day_limit")
    private Integer personDayLimit = 0;
    @Column(name = "inputed_as_number")
    @Expose
    @SerializedName("inputed_as_number")
    private Integer inputedAsNumber = 0;
    /**
     * Это ограничение в днях, в пределах которого можно записаться вперед при предварительной записи может быть null или 0 если нет ограничения.
     */
    @Column(name = "advance_limit_period")
    @Expose
    @SerializedName("advance_limit_period")
    private Integer advanceLimitPeriod = 0;
    /**
     * Деление сетки предварительной записи.
     */
    @Column(name = "advance_time_period")
    @Expose
    @SerializedName("advance_time_period")
    private Integer advanceTimePeriod = 60;
    /**
     * Способ вызова клиента юзером 1 - стандартно 2 - backoffice, т.е. вызов следующего без табло и звука, запершение только редиректом.
     */
    @Column(name = "enable")
    @Expose
    @SerializedName("enable")
    private Integer enable = 1;
    @Column(name = "seq_id")
    private Integer seqId = 0;
    /**
     * Требовать или нет от пользователя после окончания работы с клиентом по этой услуге обозначить результат этой работы выбрав пункт из словаря результатов.
     */
    @Column(name = "result_required")
    @Expose
    @SerializedName("result_required")
    private Boolean resultRequired = false;
    /**
     * Требовать или нет на пункте регистрации ввода от клиента каких-то данных перед постановкой в очередь после выбора услуги.
     */
    @Column(name = "input_required")
    @Expose
    @SerializedName("input_required")
    private Boolean inputRequired = false;
    /**
     * На главном табло вызов по услуге при наличии третьей колонке делать так, что эту третью колонку заполнять не стройкой у юзера, а введенной пользователем
     * строчкой.
     */
    @Column(name = "inputed_as_ext")
    @Expose
    @SerializedName("inputed_as_ext")
    private Boolean inputedAsExt = false;
    /**
     * Заголовок окна при вводе на пункте регистрации клиентом каких-то данных перед постановкой в очередь после выбора услуги. Также печатается на талоне рядом
     * с введенными данными.
     */
    @Column(name = "input_caption")
    @Expose
    @SerializedName("input_caption")
    private String inputCaption = "";
    /**
     * html текст информационного сообщения перед постановкой в очередь. Если этот параметр пустой, то не требуется показывать информационную напоминалку на
     * пункте регистрации.
     */
    @Column(name = "pre_info_html")
    @Expose
    @SerializedName("pre_info_html")
    private String preInfoHtml = "";
    /**
     * текст для печати при необходимости перед постановкой в очередь.
     */
    @Column(name = "pre_info_print_text")
    @Expose
    @SerializedName("pre_info_print_text")
    private String preInfoPrintText = "";
    /**
     * текст для печати при необходимости перед постановкой в очередь.
     */
    @Column(name = "ticket_text")
    @Expose
    @SerializedName("ticket_text")
    private String ticketText = "";
    /**
     * текст для вывода на главное табло в шаблоны панели вызванного и третью колонку пользователя.
     */
    @Column(name = "tablo_text")
    @Expose
    @SerializedName("tablo_text")
    private String tabloText = "";
    /**
     * Расположение кнопки на пункте регистрации.
     */
    @Column(name = "but_x")
    @Expose
    @SerializedName("but_x")
    private Integer butX = 100;
    @Column(name = "but_y")
    @Expose
    @SerializedName("but_y")
    private Integer butY = 100;
    @Column(name = "but_b")
    @Expose
    @SerializedName("but_b")
    private Integer butB = 100;
    @Column(name = "but_h")
    @Expose
    @SerializedName("but_h")
    private Integer butH = 100;
    /**
     * последний номер, выданный последнему кастомеру при номерировании клиентов обособлено в услуге. тут такой замут. когда услугу создаешь из json где-то на
     * клиенте, то там же спринг-контекст не поднят да и нужно это только в качестве данных.
     */
    @Transient
    private int lastNumber = Integer.MIN_VALUE;
    // чтоб каждый раз в бд не лазить для проверки сколько предварительных сегодня по этой услуге
    @Transient
    private int dayY = -100; // для смены дня проверки
    @Transient
    private int dayAdvs = -100; // для смены дня проверки
    /**
     * Сколько кастомеров уже прошло услугу сегодня.
     */
    @Transient
    @Expose
    @SerializedName("countPerDay")
    private int countPerDay = 0;
    /**
     * Текущий день, нужен для учета количества кастомеров обработанных в этой услуге в текущий день.
     */
    @Transient
    @Expose
    @SerializedName("day")
    private int day = 0;
    /**
     * Описание услуги.
     */
    @Expose
    @SerializedName("description")
    @Column(name = "description")
    private String description;
    /**
     * Префикс услуги.
     */
    @Expose
    @SerializedName("service_prefix")
    @Column(name = "service_prefix")
    private String prefix = "";
    /**
     * Наименование услуги.
     */
    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;
    /**
     * Надпись на кнопке услуги.
     */
    @Expose
    @SerializedName("buttonText")
    @Column(name = "button_text")
    private String buttonText;
    /**
     * Группировка услуг.
     */
    @Expose
    @SerializedName("parentId")
    @Column(name = "prent_id")
    private Long parentId;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "link_service_id")
    private QService link;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "services_id")
    @Expose
    @SerializedName("langs")
    private Set<QServiceLang> langs = new HashSet<>();
    @Transient
    private HashMap<String, QServiceLang> qslangs = null;
    /**
     * Если не NULL и не пустая, то эта услуга недоступна и сервер обламает постановку в очередь выкинув причину из этого поля на пункт регистрации.
     */
    @Transient
    private String tempReasonUnavailable;
    /**
     * По сути группа объединения услуг или коернь всего дерева. То во что включена данныя услуга.
     */
    @Transient
    private QService parentService;

    public QService() {
        super();
    }

    public static void clearNextStNumber() {
    }

    private PriorityQueue<QCustomer> getCustomers() {
        return customers;
    }

    /**
     * Это все кастомеры стоящие к этой услуге в виде списка Только для бакапа на диск.
     *
     * @return
     */
    public LinkedBlockingDeque<QCustomer> getClients() {
        return clients;
    }

    @Override
    public Long getId() {
        return id;
    }

    public final void setId(Long id) {
        this.id = id;
    }

    /**
     * Если нужно для услуги что-то сохранять в системных параметрах, то это надо сохранять в секцию для этой конкретной услуги.
     *
     * @return Имя секции в системных параметах для конкретно этой услуги.
     */
    public String getSectionName() {
        return "srv_" + id.toString();
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    /**
     * Состояние услуги. Влияет на состояние кнопки на киоске, при редиректе.
     *
     * @return 1 - доступна, 0 - недоступна, -1 - невидима, 2 - только для предвариловки, 3 - заглушка, 4 - не для предвариловки, 5 - рулон.
     */
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Пунктов регистрации может быть много. Наборы кнопок на разных киосках могут быть разные. Указание для какого пункта регистрации услуга, 0-для всех, х-для
     * киоска х.
     *
     * @return Наборы кнопок на разных киосках могут быть разные.
     */
    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    /**
     * Норматив. Среднее время оказания этой услуги. Зачем надо? Не знаю. Пока для маршрутизации при медосмотре. Может потом тоже применем.
     *
     * @return Норматив.
     */
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * Время обязательного ожидания посетителя.
     *
     * @return в минутах
     */
    public Integer getExpectation() {
        return expectation;
    }

    public void setExpectation(Integer expectation) {
        this.expectation = expectation;
    }

    /**
     * шаблон звукового приглашения. null или 0... - использовать родительский. Далее что играем а что нет.
     *
     * @return шаблон звукового приглашения.
     */
    public String getSoundTemplate() {
        return soundTemplate;
    }

    public void setSoundTemplate(String soundTemplate) {
        this.soundTemplate = soundTemplate;
    }

    public Integer getAdvanceLimit() {
        return advanceLimit;
    }

    public void setAdvanceLinit(Integer advanceLimit) {
        this.advanceLimit = advanceLimit;
    }

    public Integer getDayLimit() {
        return dayLimit;
    }

    public void setDayLimit(Integer dayLimit) {
        this.dayLimit = dayLimit;
    }

    public Integer getPersonDayLimit() {
        return personDayLimit;
    }

    public void setPersonDayLimit(Integer personDayLimit) {
        this.personDayLimit = personDayLimit;
    }

    /**
     * Если требуется использовать введенные пользователем данные как его номер в очереди. 0 - генерировать как обычно. enteredAsNumber - использовать первые
     * enteredAsNumber символов как номер в очереди
     *
     * @return 0 - генерировать как обычно. enteredAsNumber - использовать первые enteredAsNumber символов как номер в очереди.
     */
    public Integer getInputedAsNumber() {
        return inputedAsNumber;
    }

    public void setInputedAsNumber(Integer inputedAsNumber) {
        this.inputedAsNumber = inputedAsNumber;
    }

    public Integer getAdvanceLimitPeriod() {
        return advanceLimitPeriod;
    }

    public void setAdvanceLimitPeriod(Integer advanceLimitPeriod) {
        this.advanceLimitPeriod = advanceLimitPeriod;
    }

    public Integer getAdvanceTimePeriod() {
        return advanceTimePeriod;
    }

    public void setAdvanceTimePeriod(Integer advanceTimePeriod) {
        this.advanceTimePeriod = advanceTimePeriod;
    }

    /**
     * Способ вызова клиента юзером 1 - стандартно 2 - backoffice, т.е. вызов следующего без табло и звука, запершение только редиректом.
     *
     * @return int index.
     */
    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getSeqId() {
        return seqId;
    }

    public void setSeqId(Integer seqId) {
        this.seqId = seqId;
    }

    /**
     * Требовать или нет от пользователя после окончания работы с клиентом по этой услуге обозначить результат этой работы выбрав пункт из словаря результатов.
     *
     * @return да или нет.
     */
    public Boolean getResultRequired() {
        return resultRequired;
    }

    public void setResultRequired(Boolean resultRequired) {
        this.resultRequired = resultRequired;
    }

    /**
     * Требовать или нет на пункте регистрации ввода от клиента каких-то данных перед постановкой в очередь после выбора услуги.
     *
     * @return да или нет.
     */
    public Boolean getInputRequired() {
        return inputRequired;
    }

    public void setInputRequired(Boolean inputRequired) {
        this.inputRequired = inputRequired;
    }

    /**
     * На главном табло вызов по услуге при наличии третьей колонке делать так, что эту третью колонку заполнять не стройкой у юзера, а введенной пользователем
     * строчкой.
     *
     * @return Разрешение выводить на табло введеные посетителем на киоске(или еще как) данные.
     */
    public Boolean getInputedAsExt() {
        return inputedAsExt;
    }

    public void setInputedAsExt(Boolean inputedAsExt) {
        this.inputedAsExt = inputedAsExt;
    }

    /**
     * Заголовок окна при вводе на пункте регистрации клиентом каких-то данных перед постановкой в очередь после выбора услуги. Также печатается на талоне рядом
     * с введенными данными.
     *
     * @return текст заголовка
     */
    public String getInputCaption() {
        return inputCaption;
    }

    public void setInputCaption(String inputCaption) {
        this.inputCaption = inputCaption;
    }

    public String getPreInfoHtml() {
        return preInfoHtml;
    }

    public void setPreInfoHtml(String preInfoHtml) {
        this.preInfoHtml = preInfoHtml;
    }

    public String getPreInfoPrintText() {
        return preInfoPrintText;
    }

    public void setPreInfoPrintText(String preInfoPrintText) {
        this.preInfoPrintText = preInfoPrintText;
    }

    public String getTicketText() {
        return ticketText;
    }

    public void setTicketText(String ticketText) {
        this.ticketText = ticketText;
    }

    /**
     * текст для вывода на главное табло в шаблоны панели вызванного и третью колонку пользователя.
     *
     * @return строчеп из БД.
     */
    public String getTabloText() {
        return tabloText;
    }

    public void setTabloText(String tabloText) {
        this.tabloText = tabloText;
    }

    public Integer getButB() {
        return butB;
    }

    public void setButB(Integer butB) {
        this.butB = butB;
    }

    public Integer getButH() {
        return butH;
    }

    public void setButH(Integer butH) {
        this.butH = butH;
    }

    public Integer getButX() {
        return butX;
    }

    public void setButX(Integer butX) {
        this.butX = butX;
    }

    public Integer getButY() {
        return butY;
    }

    public void setButY(Integer butY) {
        this.butY = butY;
    }

    @Override
    public String toString() {
        return getName().trim().isEmpty() ? "<NO_NAME>" : getName();
    }

    // ***************************************************************************************
    // ********************  МЕТОДЫ УПРАВЛЕНИЯ ЭЛЕМЕНТАМИ И СТРУКТУРЫ ************************
    // ***************************************************************************************

    /**
     * Получить номер для сделующего кастомера. Произойдет инкремент счетчика номеров.
     *
     * @return номер следующего талона.
     */
    public int getNextNumber() {
        synchronized (QService.class) {
            return ++lastNumber;

        }
    }

    /**
     * Узнать сколько предварительно записанных для этой услуги на дату.
     *
     * @param date        на эту дату узнаем количество записанных предварительно
     * @param strictStart false - просто количество записанных на этот день, true - количество записанных на этот день начиная с времени date
     * @return количество записанных предварительно
     */
    public int getAdvancedCount(Date date, boolean strictStart) {
        return 0;
    }

    /**
     * Иссяк лимит на одинаковые введенные данные в день по услуге или нет.
     *
     * @param data что-то.
     * @return true - превышен, в очередь становиться нельзя; false - можно в очередь встать
     */
    public boolean isLimitPersonPerDayOver(String data) {
        final int today = new GregorianCalendar().get(GregorianCalendar.DAY_OF_YEAR);
        if (today != day) {
            day = today;
            setCountPerDay(0);
        }
        return getPersonDayLimit() != 0 && getPersonDayLimit() <= getCountPersonsPerDay(data);
    }

    private int getCountPersonsPerDay(String data) {
        int cnt = 0;
        return cnt;
    }

    /**
     * Иссяк лимит на возможных обработанных в день по услуге или нет.
     *
     * @return true - превышен, в очередь становиться нельзя; false - можно в очередь встать.
     */
    public boolean isLimitPerDayOver() {
        return false;
    }

    /**
     * Получить количество талонов, которые все еще можно выдать учитывая ограничение на время работы с одним клиетом.
     *
     * @return оставшееся время работы по услуге / ограничение на время работы с одним клиетом.
     */
    public long getPossibleTickets() {
        return 100;
    }

    public int getCountPerDay() {
        return countPerDay;
    }

    public void setCountPerDay(int countPerDay) {
        this.countPerDay = countPerDay;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    /**
     * Сбросить нумерацию талонов. Номер станет на единицу меньше настройки и при получении следующего будет выдан начальный номер талона.
     * Если есть настройка srv_[serviceId].ticket_numbering = "from [number] to [number]" то применится она. Иначе настройка из админки.
     */
    public void clearNextNumber() {
    }

    /**
     * Можно изменить текущий номер талона. Это нужно если у вас услуга-рулон. Повесили новый пулон, там на конце болтается какой-то номер, его выставили
     * текущим.
     *
     * @param newCurrent этот и будет текущим.
     */
    public void reinitNextNumber(int newCurrent) {
        lastNumber = newCurrent - 1;
    }

    /**
     * Привосстановлении ситуации из сохраненного файла. Сохраненный кастомер стоял в очереди и ждал, но его еще никто не звал.
     *
     * @param customer сохраненный.
     */
    public void addCustomerForRecoveryOnly(QCustomer customer) {
    }

    /**
     * Добавить в очередь при этом проставится название сервиса, в который всрал, и его описание, если у кастомера нету префикса, то проставится и префикс.
     *
     * @param customer это кастомер которого добавляем в очередь к услуге
     */
    public void addCustomer(QCustomer customer) {
        if (customer.getPrefix() == null) {
            customer.setPrefix(getPrefix());
        }
        if (!getCustomers().add(customer)) {
            throw new ServerException("Невозможно добавить нового кастомера в хранилище кастомеров.");
        }

        // поддержка расширяемости плагинами/ определим куда влез клиент
        QCustomer before = null;
        QCustomer after = null;
        for (Iterator<QCustomer> itr = getCustomers().iterator(); itr.hasNext(); ) {
            final QCustomer c = itr.next();
            if (!customer.getId().equals(c.getId())) {
                if (customer.compareTo(c) > 0) {
                    // c - первее, определяем before
                    if (before == null) {
                        before = c;
                    } else {
                        if (before.compareTo(c) < 0) {
                            before = c;
                        }
                    }
                } else {
                    if (customer.compareTo(c) != 0) {
                        // c - после, определяем after
                        if (after == null) {
                            after = c;
                        } else {
                            if (after.compareTo(c) > 0) {
                                after = c;
                            }
                        }
                    }
                }
            }
        }

        clients.clear();
        clients.addAll(getCustomers());
    }

    /**
     * Всего хорошего, все свободны!.
     */
    public void freeCustomers() {

        getCustomers().clear();
        clients.clear();
        clients.addAll(getCustomers());
    }

    /**
     * Получить, но не удалять. NoSuchElementException при неудаче.
     *
     * @return первого в очереди кастомера.
     */
    public QCustomer getCustomer() {
        return getCustomers().element();
    }

    /**
     * Получить и удалить. NoSuchElementException при неудаче.
     *
     * @return первого в очереди кастомера.
     */
    public QCustomer removeCustomer() {
        final QCustomer customer = getCustomers().remove();

        clients.clear();
        clients.addAll(getCustomers());
        return customer;
    }

    /**
     * Удалить любого в очереди кастомера.
     *
     * @param customer удаляемый кастомер.
     * @return может вернуть false при неудаче.
     */
    public boolean removeCustomer(QCustomer customer) {
        final Boolean res = getCustomers().remove(customer);

        clients.clear();
        clients.addAll(getCustomers());
        return res;
    }

    /**
     * Получить но не удалять. null при неудаче.
     *
     * @return первого в очереди кастомера.
     */
    public QCustomer peekCustomer() {
        return getCustomers().peek();
    }

    /**
     * Получить и удалить. может вернуть null при неудаче.
     *
     * @return первого в очереди кастомера.
     */
    public QCustomer polCustomer() {
        final QCustomer customer = getCustomers().poll();

        clients.clear();
        clients.addAll(getCustomers());
        return customer;
    }

    /**
     * Получение количества кастомеров, стоящих в очереди.
     *
     * @return количество кастомеров в этой услуге.
     */
    public int getCountCustomers() {
        return getCustomers().size();
    }

    /**
     * Определим общую длмну очереди к этой услуге. Т.е. сколько реально будет вызвано разных посетителей, которые мешают сразу попасть именно к этой услуге.
     * <br>
     * Бежим по юзерам и смотрим обрабатывают ли они услугу <br>
     * если да, то возьмем все услуги юзера и сложим всех кастомеров в очередях <br>
     * самую маленькую сумму отправим в ответ по запросу.
     *
     * @return размер толпы перед попаданием к этой услуге.
     */
    public int getQueueSize() {
        // бежим по юзерам и смотрим обрабатывают ли они услугу
        // если да, то возьмем все услуги юзера и  сложим всех кастомеров в очередях
        // самую маленькую сумму отправим в ответ по запросу.
        int min = Integer.MAX_VALUE;
        return min == Integer.MAX_VALUE ? 0 : min;
    }

    /**
     * Поменяем приоритет по номеру.
     *
     * @param number      номер
     * @param newPriority новый приоритет.
     * @return да или нет.
     */
    public boolean changeCustomerPriorityByNumber(String number, int newPriority) {
        for (QCustomer customer : getCustomers()) {
            if (number.equalsIgnoreCase(customer.getPrefix() + (customer.getNumber() < 1 ? "" : customer.getNumber()))) {
                customer.setPriority(newPriority);
                removeCustomer(customer); // убрать из очереди
                addCustomer(customer);// перепоставили чтобы очередность переинлексиловалась
                return true;
            }
        }
        return false;
    }

    /**
     * Выгрызть кастомера по ID. При нехороших действиях.
     *
     * @param number этого грызем.
     * @return выгрызыный.
     */
    public QCustomer gnawOutCustomerByNumber(String number) {
        for (QCustomer customer : getCustomers()) {
            if (number.equalsIgnoreCase(customer.getPrefix() + (customer.getNumber() < 1 ? "" : customer.getNumber()))) {
                removeCustomer(customer); // убрать из очереди
                return customer;
            }
        }
        return null;
    }

    /**
     * Выгрызть кастомера по ID. При нехороших действиях.
     *
     * @param customerId этого грызем.
     * @return выгрызыный.
     */
    public QCustomer gnawOutCustomerById(Long customerId) {
        for (QCustomer customer : getCustomers()) {
            if (customerId.equals(customer.getId())) {
                removeCustomer(customer); // убрать из очереди
                return customer;
            }
        }
        return null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrefix() {
        return prefix == null ? "" : prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix == null ? "" : prefix;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    @Override
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public QService getLink() {
        return link;
    }

    public void setLink(QService link) {
        this.link = link;
    }

    public Set<QServiceLang> getLangs() {
        return langs;
    }

    public void setLangs(Set<QServiceLang> langs) {
        this.langs = langs;
    }
    //*******************************************************************************************************************
    //*******************************************************************************************************************
    //********************** Реализация методов узла в дереве ***********************************************************

    /**
     * Прлучить язык с текстами для услуги.
     *
     * @param nameLocale этой локали.
     */
    public QServiceLang getServiceLang(String nameLocale) {
        if (qslangs == null) {
            qslangs = new HashMap<>();
            getLangs().stream().forEach(sl -> qslangs.put(sl.getLang(), sl));
        }
        return qslangs.get(nameLocale);
    }

    public String getTempReasonUnavailable() {
        return tempReasonUnavailable;
    }

    public void setTempReasonUnavailable(String tempReasonUnavailable) {
        this.tempReasonUnavailable = tempReasonUnavailable;
    }

    @SuppressWarnings("squid:S1319")
    public LinkedList<QService> getChildren() {
        return childrenOfService;
    }

    @Override
    public void addChild(ITreeIdGetter child) {
        if (!childrenOfService.contains(child)) { // бывает что добавляем повторно ужедобавленный
            childrenOfService.add((QService) child);
        }
    }

    @Override
    public QService getChildAt(int childIndex) {
        return childrenOfService.get(childIndex);
    }

    @Override
    public int getChildCount() {
        return childrenOfService.size();
    }

    @Override
    public QService getParent() {
        return parentService;
    }

    @Override
    public void setParent(MutableTreeNode newParent) {
        parentService = (QService) newParent;
        if (parentService != null) {
            setParentId(parentService.id);
        } else {
            parentId = null;
        }
    }

    @Override
    public int getIndex(TreeNode node) {
        return childrenOfService.indexOf(node);
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    @Override
    public boolean isLeaf() {
        return getChildCount() == 0;
    }

    @Override
    public Enumeration children() {
        return Collections.enumeration(childrenOfService);
    }

    @Override
    public void insert(MutableTreeNode child, int index) {
        child.setParent(this);
        this.childrenOfService.add(index, (QService) child);
    }

    @Override
    public void remove(int index) {
        this.childrenOfService.remove(index);
    }

    @Override
    public void remove(MutableTreeNode node) {
        this.childrenOfService.remove(node);
    }

    @Override
    public void removeFromParent() {
        getParent().remove(getParent().getIndex(this));
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return flavors;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        return true;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if (this.isDataFlavorSupported(flavor)) {
            return this;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }
    }

    public enum Field {

        /**
         * Надпись на кнопке.
         */
        BUTTON_TEXT,
        /**
         * заголовок ввода клиентом.
         */
        INPUT_CAPTION,
        /**
         * читаем перед тем как встать в очередь.
         */
        PRE_INFO_HTML,
        /**
         * печатаем подсказку перед тем как встать в очередь.
         */
        PRE_INFO_PRINT_TEXT,
        /**
         * текст на талоте персонально услуги.
         */
        TICKET_TEXT,
        /**
         * описание услуги.
         */
        DESCRIPTION,
        /**
         * имя услуги.
         */
        NAME
    }
}