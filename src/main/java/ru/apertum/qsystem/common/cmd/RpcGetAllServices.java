/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.common.cmd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.apertum.qsystem.server.model.QService;

import java.util.Date;

/**
 * @author Evgeniy Egorov
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class RpcGetAllServices extends JsonRPC20 {

    @Expose
    @SerializedName("result")
    private ServicesForWelcome result;

    public RpcGetAllServices() {
    }

    public RpcGetAllServices(ServicesForWelcome result) {
        this.result = result;
    }

    @Data
    public static class ServicesForWelcome {

        @Expose
        @SerializedName("root")
        private QService root;
        @Expose
        @SerializedName("start_time")
        private Date startTime;
        @Expose
        @SerializedName("finish_time")
        private Date finishTime;

        public ServicesForWelcome() {
        }

        public ServicesForWelcome(QService root, Date startTime, Date finishTime) {
            this.root = root;
            this.startTime = startTime;
            this.finishTime = finishTime;
        }
    }
}
