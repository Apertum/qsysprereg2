/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.common.cmd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Evgeniy Egorov
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class RpcGetGridOfWeek extends JsonRPC20 {

    @Expose
    @SerializedName("result")
    private GridAndParams result;

    public RpcGetGridOfWeek() {
    }

    public RpcGetGridOfWeek(GridAndParams result) {
        this.result = result;
    }

    @Data
    public static class GridAndParams {

        /**
         * Костыль. Если вместо данных возвращается строка с текстом ошибки.
         */
        @Expose
        @SerializedName("error")
        private String spError;
        @Expose
        @SerializedName("times")
        private List<Date> times;
        @Expose
        @SerializedName("start")
        private Date startTime;
        @Expose
        @SerializedName("finish")
        private Date finishTime;
        @Expose
        @SerializedName("limit")
        private int advanceLimit;
        @Expose
        @SerializedName("limit_period")
        private int advanceLimitPeriod;
        @Expose
        @SerializedName("limit_time")
        private int advanceTimePeriod;

        public GridAndParams(String spError) {
            this.spError = spError;
        }

        public GridAndParams() {
        }

        public void addTime(Date time) {
            if (times == null) {
                times = new LinkedList<>();
            }
            times.add(time);
        }
    }
}
