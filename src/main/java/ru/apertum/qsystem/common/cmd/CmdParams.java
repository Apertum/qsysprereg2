/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package ru.apertum.qsystem.common.cmd;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Evgeniy Egorov
 */
@Data
public class CmdParams {

    private static Logger log = LogManager.getLogger(CmdParams.class);

    public static final String CMD = "cmd";

    public CmdParams() {
    }

    public CmdParams(String params) {
        initFromString(params);
    }

    @Expose
    @SerializedName("service_id")
    private Long serviceId;
    @Expose
    @SerializedName("user_id")
    private Long userId;
    @Expose
    @SerializedName("pass")
    private String parolcheg;
    @Expose
    @SerializedName("priority")
    private Integer priority;
    @Expose
    @SerializedName("text_data")
    private String textData;
    @Expose
    @SerializedName("result_id")
    private Long resultId;
    @Expose
    @SerializedName("request_back")
    private Boolean requestBack;
    @Expose
    @SerializedName("drop_tickets_cnt")
    private Boolean dropTicketsCounter;
    /**
     * В качестве признака персональности едет ID юзера, чей отложенный должен быть, иначе null для общедоступности.
     */
    @Expose
    @SerializedName("is_only_mine")
    private Long isMine;
    @Expose
    @SerializedName("strict")
    private Boolean strict;
    @Expose
    @SerializedName("coeff")
    private Integer coeff;
    @Expose
    @SerializedName("date")
    private Long date;
    @Expose
    @SerializedName("customer_id")
    private Long customerId;
    @Expose
    @SerializedName("response_id")
    private Long responseId;
    @Expose
    @SerializedName("client_auth_id")
    private String clientAuthId;
    @Expose
    @SerializedName("info_item_name")
    private String infoItemName;
    @Expose
    @SerializedName("postponed_period")
    private Integer postponedPeriod;
    @Expose
    @SerializedName("comments")
    private String comments;
    @Expose
    @SerializedName("first_in_roll")
    private Integer first;
    @Expose
    @SerializedName("lastt_in_roll")
    private Integer last;
    @Expose
    @SerializedName("currentt_in_roll")
    private Integer current;
    @Expose
    @SerializedName("lng")
    private String language;
    /**
     * услуги, в которые пытаемся встать. Требует уточнения что это за трехмерный массив. Это пять списков. Первый это вольнопоследовательные услуги. Остальные
     * четыре это зависимопоследовательные услуги, т.е. пока один не закончится на другой не переходить. Что такое элемент списка. Это тоже список. Первый
     * элемент это та самая комплексная услуга(ID). А остальные это зависимости, т.е. если есть еще не оказанные услуги но назначенные, которые в зависимостях,
     * то их надо оказать.
     */
    @Expose
    @SerializedName("complex_id")
    private LinkedList<LinkedList<LinkedList<Long>>> complexId; // NOSONAR

    /**
     * Просто ассоциированный массив строк для передачи каких-то параметров. Пригодится для плагинов.
     */
    @Expose
    @SerializedName("strings_map")
    private Map<String, String> stringsMap;

    @SuppressWarnings("squid:S3776")
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("^?");
        final Field[] fs = getClass().getDeclaredFields();
        try {
            for (Field field : fs) {
                if (!(Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers()))
                        && field.get(this) != null) {

                    field.setAccessible(true);//NOSONAR

                    if (field.getType().getSimpleName().endsWith("Map")) {
                        final Map map = (Map) field.get(this);
                        if (map.size() > 0) {
                            sb.append("&").append(field.getName()).append("=");
                            map.keySet().stream().forEach(object -> {
                                try {
                                    sb.append(URLEncoder.encode("{" + object.toString() + "->" + map.get(object) + "}", UTF_8.name()));
                                } catch (UnsupportedEncodingException ex) {
                                    log.error(ex);
                                }
                            });
                        }
                    } else {

                        switch (field.getType().getSimpleName().toLowerCase(Locale.US)) {
                            case "int":
                                sb.append("&").append(field.getName()).append("=").append(field.get(this));
                                break;
                            case "integer":
                                sb.append("&").append(field.getName()).append("=").append(field.get(this));
                                break;
                            case "string":
                                sb.append("&").append(field.getName()).append("=").append(URLEncoder.encode((String) field.get(this), UTF_8.name()));
                                break;
                            case "boolean":
                                sb.append("&").append(field.getName()).append("=").append(field.get(this));
                                break;
                            case "long":
                                sb.append("&").append(field.getName()).append("=").append(field.get(this));
                                break;
                            case "logger":
                                break;
                            default:
                                throw new AssertionError("Type '" + field.getType().getSimpleName() + "' was not found in fields of class '" + getClass() + "'.");
                        }
                    }
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException | UnsupportedEncodingException ex) {
            log.error(ex);
        }

        final String st = sb.toString().replaceFirst("^\\^\\?\\&", "");
        sb.setLength(0);
        return st.length() < 3 ? "" : st;
    }

    /**
     * Загрузим из строки.
     *
     * @param params строка с параметрами.
     */
    @SuppressWarnings("squid:S3776")
    public final void initFromString(String params) {
        if (params == null || params.isEmpty()) {
            return;
        }
        for (String str : params.split("&")) {
            final String[] pp = str.split("=");

            final Field[] fs = getClass().getDeclaredFields();
            try {
                for (Field field : fs) {

                    if (!(Modifier.isStatic(field.getModifiers()) && Modifier.isFinal(field.getModifiers()))
                            && pp[0].equals(field.getName())) {

                        field.setAccessible(true);//NOSONAR

                        switch (field.getType().getSimpleName().toLowerCase(Locale.US)) {
                            case "int":
                                field.set(this, Integer.parseInt(pp[1]));//NOSONAR
                                break;
                            case "integer":
                                field.set(this, Integer.parseInt(pp[1]));//NOSONAR
                                break;
                            case "string":
                                field.set(this, pp.length == 1 ? "" : URLDecoder.decode(pp[1], UTF_8.name()));//NOSONAR
                                break;
                            case "boolean":
                                field.set(this, Boolean.parseBoolean(pp[1]));//NOSONAR
                                break;
                            case "long":
                                field.set(this, Long.parseLong(pp[1]));//NOSONAR
                                break;
                            case "map":
                                final HashMap<String, String> map = new HashMap<>();
                                final String s = URLDecoder.decode(pp[1], UTF_8.name());
                                final String[] ss = s.split("\\{|\\}");
                                for (String s1 : ss) {
                                    final String[] ss1 = s1.split("->");
                                    if (ss1.length == 2) {
                                        map.put(ss1[0], ss1[1]);
                                    }
                                }
                                field.set(this, map);//NOSONAR
                                break;
                            default:
                                throw new AssertionError();
                        }
                    }
                }
            } catch (IllegalArgumentException | IllegalAccessException | UnsupportedEncodingException ex) {
                log.error(ex);
            }
        }
    }
}
