package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import ru.apertum.qsystem.prereg.core.AppConfig;
import ru.apertum.qsystem.prereg.core.Config;

import java.util.Arrays;
import java.util.List;

@Log4j2
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ClientForm extends Index {

    private final RandomStringGenerator rsg = new RandomStringGenerator(5);

    @WireVariable("cnf")
    private AppConfig appConfig;

    @WireVariable("config")
    private Config config;

    @WireVariable("client")
    private Client client;

    @Getter
    @Setter
    private Client clientTmp = new Client();

    @Getter
    @Setter
    private String captcha = rsg.getRandomString();

    @Getter
    @Setter
    private String captchaInput;

    @Getter
    @Setter
    private String foregroundColour = "#000000";

    @Getter
    @Setter
    private String backgroundColour = "#FDC966";

    @Init
    @Override
    public void init() {
        super.init();
        String com = Executions.getCurrent().getParameter("com");
        appConfig.setCom(com);
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        Selectors.wireEventListeners(view, this);
    }

    public void regenerateCaptcha() {
        this.captcha = rsg.getRandomString();
    }

    @Command
    @NotifyChange("captcha")
    public void regenerate() {
        this.regenerateCaptcha();
    }

    @Command
    public void submit() {
        try {
            client.initClient(clientTmp);
            Executions.sendRedirect("~./selectService.zul");
        } catch (Exception t) {
            log.error("Server QMS is down!. ", t);
            Executions.sendRedirect("~./error.zul");
        }
    }


    @Listen("onClick = #consentDetailedBtn")
    public void showModal(Event e) {
        Executions.getCurrent().sendRedirect(config.getConsentDetailedLink(), "_blank");
    }
}
