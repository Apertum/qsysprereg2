package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import ru.apertum.qsystem.prereg.core.ServiceList;
import ru.apertum.qsystem.server.model.QService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
@Log4j2
public class ServiceForm {

    @WireVariable("services")
    @Getter
    @Setter
    private ServiceList serviceList;

    @WireVariable("client")
    @Getter
    @Setter
    private Client client;

    @Getter
    private QService selectedService;

    public void setSelectedService(QService selectedService) {
        selectedService.setInputCaption(selectedService.getInputCaption().replaceAll("<.*?>", ""));
        this.selectedService = selectedService;
    }

    @Command
    public void back() {
        Executions.sendRedirect("/");
    }

    @Command
    public void submit() {
        if (selectedService != null) {
            log.info("Selected service {};  ID={}; Caption: {}", selectedService.getName(), selectedService.getId(), selectedService.getInputCaption());
            client.setService(selectedService);
            Executions.sendRedirect(selectedService.getInputRequired() ? "~./inputServiceData.zul" : "~./selectTime.zul");
        }
    }
}
