package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import ru.apertum.qsystem.common.ServerException;

import java.util.Random;

public class RandomStringGenerator {
    private final Random rn = new Random();

    @Getter
    @Setter
    private int length;

    private static final String ALPHABET = "1234567890";

    public RandomStringGenerator(int length) {
        if (length <= 0) {
            throw new ServerException("Length cannot be less than or equal to 0");
        }

        this.length = length;
    }

    public String getRandomString() {
        StringBuilder sb = new StringBuilder(this.length);

        for (int i = 0; i < this.length; i++) {
            sb.append(ALPHABET.charAt(rn.nextInt(ALPHABET.length())));
        }

        return sb.toString();
    }
}
