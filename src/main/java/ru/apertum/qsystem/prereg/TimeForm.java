package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Calendar;
import ru.apertum.qsystem.common.QException;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.common.cmd.RpcGetGridOfWeek;
import ru.apertum.qsystem.common.model.QAdvanceCustomer;
import ru.apertum.qsystem.prereg.core.AppConfig;
import ru.apertum.qsystem.prereg.core.Config;
import ru.apertum.qsystem.prereg.core.Mailer;
import ru.apertum.qsystem.prereg.core.Multilingual;
import ru.apertum.qsystem.prereg.core.NetCommander;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static ru.apertum.qsystem.prereg.core.Multilingual.ARMENIAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.AZERBAIJAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.RUSSIAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.UKRAINIAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.UKRAINIAN_MONAT_WEEKDAYS;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
@Log4j2
public class TimeForm extends Index {

    @Getter
    private final List<DateStr> timeList = new LinkedList<>();

    public static class DateStr {

        @Getter
        final Date date;

        final Locale locale;

        DateStr(Date date, Locale locale) {
            this.date = date;
            this.locale = locale;
        }

        @Override
        public String toString() {
            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(locale);
            switch (locale.toString().toLowerCase(Locale.US)) {
                case "ru_ru":
                    dateFormatSymbols.setMonths(RUSSIAN_MONAT);
                    break;
                case "uk_ua":
                    dateFormatSymbols.setMonths(UKRAINIAN_MONAT);
                    dateFormatSymbols.setWeekdays(UKRAINIAN_MONAT_WEEKDAYS);
                    break;
                case "az_az":
                    dateFormatSymbols.setMonths(AZERBAIJAN_MONAT);
                    break;
                case "hy_am":
                    dateFormatSymbols.setMonths(ARMENIAN_MONAT);
                    break;
                default:
                    log.trace("No cpec monats.");
            }
            return new SimpleDateFormat("EEEE dd MMMM  HH:mm", dateFormatSymbols).format(date);
        }
    }

    @WireVariable("lng")
    private Multilingual lng;

    @WireVariable("net")
    private NetCommander netCommander;

    @WireVariable("cnf")
    private AppConfig appConfig;

    @WireVariable("config")
    private Config config;

    @WireVariable("email")
    private Mailer mailer;

    @WireVariable("client")
    private Client client;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Getter
    @Setter
    private DateStr selectedDate;

    @Getter
    @Setter
    private Date date = new Date();

    @Wire("#cal")
    @Getter
    @Setter
    private Calendar cal;

    @Getter
    @Setter
    private String strictDate = "20591010";

    @Init
    @Override
    public void init() {
        super.init();
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(GregorianCalendar.DAY_OF_YEAR, client.getService().getAdvanceLimitPeriod());
        setStrictDate(new SimpleDateFormat("yyyyMMdd").format(gc.getTime()));
    }

    @Command
    public void submit() {
        if (selectedDate != null) {
            log.debug("Date {} was selected.", selectedDate);
            // ставим предварительного кастомера
            final QAdvanceCustomer result;
            try {
                result = netCommander.standInServiceAdvance(appConfig.getNetProperty(), client.getService().getId(), selectedDate.getDate(), -1, selectInputed(), createComment());
            } catch (QException e) {
                throw new ServerException(e);
            }
            client.setAdvClient(result);
            client.setDate(selectedDate.getDate());
            if (client.getEmail() != null && !client.getEmail().isEmpty()) {
                try {
                    mailer.sendReporterMail(client);
                } catch (Exception ex) {
                    log.error("[ERROR] Mail was not sent: ", ex);
                }
            }
            Executions.sendRedirect("~./finish.zul");
        }
    }

    @Command
    public void back() {
        Executions.sendRedirect("~./selectService.zul");
    }

    @Command
    @NotifyChange("timeList")
    public void changeDate() {
        RpcGetGridOfWeek.GridAndParams gp;
        try {
            gp = netCommander.getGridOfWeek(appConfig.getNetProperty(), client.getService().getId(), date, -1);
        } catch (Exception ex) {
            throw new ServerException("Bad net conversation: " + ex);
        }
        timeList.clear();
        if (gp.getSpError() == null) {
            final GregorianCalendar gcSt = new GregorianCalendar();
            gcSt.setTime(gp.getStartTime());
            final GregorianCalendar gcFin = new GregorianCalendar();
            gcFin.setTime(gp.getFinishTime());

            final GregorianCalendar gc = new GregorianCalendar();
            final GregorianCalendar gc1 = new GregorianCalendar();
            gc.setTime(date);
            for (Date d : gp.getTimes()) {
                gc1.setTime(d);
                if (gc.get(GregorianCalendar.DAY_OF_YEAR) == gc1.get(GregorianCalendar.DAY_OF_YEAR)
                        && gc1.get(GregorianCalendar.HOUR_OF_DAY) > gcSt.get(GregorianCalendar.HOUR_OF_DAY)
                        && gc1.get(GregorianCalendar.HOUR_OF_DAY) < gcFin.get(GregorianCalendar.HOUR_OF_DAY)) {
                    timeList.add(new DateStr(d, lng.getCurrentLng().getLocale()));
                }
            }
        } else {
            log.error("Error: {}", gp.getSpError());
        }
    }

    private String selectInputed() {
        if (client.getInputData() == null || client.getInputData().isEmpty()) {
            if (config.isFieldNameAsInputed()) {
                return client.getSourname() + " " + client.getName() + " " + client.getMiddlename();
            }
            if (config.isFieldEmailAsInputed()) {
                return client.getEmail();
            }
            if (config.isFieldPhoneAsInputed()) {
                return client.getPhone();
            }
            if (config.isField1AsInputed()) {
                return client.getField1();
            }
            if (config.isField2AsInputed()) {
                return client.getField2();
            }
            if (config.isField3AsInputed()) {
                return client.getField3();
            }
            if (config.isField4AsInputed()) {
                return client.getField4();
            }
            if (config.isField5AsInputed()) {
                return client.getField5();
            }
        } else {
            return client.getInputData();
        }
        return "";
    }

    private String createComment() {
        StringBuilder sb = new StringBuilder();
        if (config.isFieldNameEnable()) {
            sb.append(client.getSourname()).append(" ").append(client.getName()).append(" ").append(client.getMiddlename()).append("; ");
        }
        if (config.isFieldEmailEnable()) {
            sb.append(client.getEmail()).append("; ");
        }
        if (config.isFieldPhoneEnable()) {
            sb.append(client.getPhone()).append("; ");
        }
        if (config.isField1Enable()) {
            sb.append(config.getField1Caption()).append(" ").append(client.getField1()).append("; ");
        }
        if (config.isField2Enable()) {
            sb.append(config.getField2Caption()).append(" ").append(client.getField2()).append("; ");
        }
        if (config.isField3Enable()) {
            sb.append(config.getField3Caption()).append(" ").append(client.getField3()).append("; ");
        }
        if (config.isField4Enable()) {
            sb.append(config.getField4Caption()).append(" ").append(client.getField4()).append("; ");
        }
        if (config.isField5Enable()) {
            sb.append(config.getField5Caption()).append(" ").append(client.getField5()).append("; ");
        }
        return sb.toString();
    }
}
