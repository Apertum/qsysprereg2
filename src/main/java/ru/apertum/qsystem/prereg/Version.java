package ru.apertum.qsystem.prereg;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author Evgeniy Egorov
 */
public class Version {

    private static Logger log = LogManager.getLogger(Version.class);

    public static void print() {
        Version version = new Version();

        log.info("====================================================================");
        log.info("***                  QSystem pre-registration 2                  ***");
        log.info("********************************************************************");
        log.info("                        Version \"{}\"", version.getVer());
        log.info("                        Date {}", version.getDate());
        log.info("********************************************************************");
        log.info("***                      QMS Apertum-QSystem                     ***");
        log.info("====================================================================");
    }

    private Version() {
        try {
            final Properties settings = new Properties();
            InputStream inStream = settings.getClass().getResourceAsStream("/BOOT-INF/classes/qsysprereg2.properties");
            if (inStream == null) {
                inStream = settings.getClass().getResourceAsStream("/qsysprereg2.properties");
            }
            if (inStream != null) {
                settings.load(inStream);
                ver = settings.getProperty("version");
                date = settings.getProperty("date");
                inStream.close();
            }
        } catch (Exception ex) {
            log.error("Cant read version. ", ex);
        }
    }

    @Getter
    private String ver = "no";

    @Getter
    private String date = "no";
}
