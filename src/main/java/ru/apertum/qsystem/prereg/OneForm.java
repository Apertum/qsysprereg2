package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Calendar;
import org.zkoss.zul.Combobox;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.common.cmd.RpcGetGridOfWeek;
import ru.apertum.qsystem.common.model.QAdvanceCustomer;
import ru.apertum.qsystem.prereg.core.Mailer;
import ru.apertum.qsystem.prereg.core.Multilingual;
import ru.apertum.qsystem.prereg.core.NetCommander;
import ru.apertum.qsystem.prereg.core.ServiceList;
import ru.apertum.qsystem.prereg.core.TreeServices;
import ru.apertum.qsystem.server.model.QService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TreeSet;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
@Log4j2
public class OneForm extends Index {

    private final RandomStringGenerator rsg = new RandomStringGenerator(5);

    @Getter
    private final TreeSet<Date> timeList = new TreeSet<>();

    @WireVariable("net")
    private NetCommander netCommander;

    @WireVariable("services")
    private ServiceList serviceList;

    @WireVariable("email")
    private Mailer mailer;

    @WireVariable("client")
    @Getter
    @Setter
    private Client client;

    @Getter
    @Setter
    private String strictDate = "20591030";

    @Getter
    @Setter
    private String captcha = rsg.getRandomString();

    @Getter
    @Setter
    private String captchaInput;

    @Getter
    @Setter
    private String foregroundColour = "#000000";

    @Getter
    @Setter
    String backgroundColour = "#FDC966";

    @Getter
    private Boolean done = false;

    @Getter
    private Boolean fail = false;
    //********************************************************************************************************************************************
    //** Древовидный комбобокс
    //********************************************************************************************************************************************
    @Getter
    private TreeServices treeServs;

    @Getter
    private QService pickedRedirectServ;

    @Wire("#cbTime")
    private Combobox cbTime;

    @Wire("#cal")
    private Calendar cal;

    @Getter
    @Setter
    private Date selectedDate;

    @Getter
    @Setter
    private Date date = new Date();

    @Getter
    @Setter
    private String inputData = "";

    @Getter
    private String inputDataRequiredText = "";

    @Getter
    private String dateTimeRequiredText = "";

    @Getter
    private String mailResult = l("send_mail");

    public ServiceList getServiceList() {
        return serviceList;
    }

    @Init
    @Override
    public void init() {
        super.init();
        final String com = Executions.getCurrent().getParameter("com");
        serviceList.getAppConfig().setCom(com);
        treeServs = new TreeServices(serviceList.getRoot());
        done = false;
        fail = false;
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        Selectors.wireComponents(view, this, false);
        Selectors.wireEventListeners(view, this);
    }

    public void regenerateCaptcha() {
        this.captcha = rsg.getRandomString();
    }

    @Command
    @NotifyChange("captcha")
    public void regenerate() {
        this.regenerateCaptcha();
    }

    public boolean getInputDataRequired() {
        return pickedRedirectServ != null && pickedRedirectServ.getInputRequired();
    }

    public boolean getPreInfoHtmtRequired() {
        return pickedRedirectServ != null && pickedRedirectServ.getPreInfoHtml() != null && !pickedRedirectServ.getPreInfoHtml().isEmpty();
    }

    public boolean getPicked() {
        return pickedRedirectServ != null && !getDone() && !getFail();
    }

    @NotifyChange({"timeList", "inputDataRequired", "preInfoHtmtRequired", "pickedRedirectServ", "client", "picked", "date", "strictDate", "selectedDate"})
    public void setPickedRedirectServ(QService pickedRedirectServ) {
        this.pickedRedirectServ = pickedRedirectServ;
        client.setService(pickedRedirectServ);
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(GregorianCalendar.DAY_OF_YEAR, pickedRedirectServ.getAdvanceLimitPeriod());
        setStrictDate(new SimpleDateFormat("yyyyMMdd").format(gc.getTime()));
        cal.setConstraint("no past, no today, before " + getStrictDate());
        pickedRedirectServ.setInputCaption(pickedRedirectServ.getInputCaption().replaceAll("<.*?>", ""));
    }

    @Command
    @NotifyChange({"timeList", "selectedDate"})
    public void changeDate() {
        RpcGetGridOfWeek.GridAndParams gp;
        try {
            gp = netCommander.getGridOfWeek(serviceList.getAppConfig().getNetProperty(), pickedRedirectServ.getId(), date, -1);
        } catch (Exception ex) {
            throw new ServerException("Bad net conversation: " + ex);
        }
        timeList.clear();
        selectedDate = null;
        cbTime.setSelectedIndex(-1);
        if (gp.getSpError() == null) {
            final GregorianCalendar gcSt = new GregorianCalendar();
            gcSt.setTime(gp.getStartTime());
            final GregorianCalendar gcFin = new GregorianCalendar();
            gcFin.setTime(gp.getFinishTime());

            final GregorianCalendar gc = new GregorianCalendar();
            final GregorianCalendar gc1 = new GregorianCalendar();
            gc.setTime(date);
            for (Date d : gp.getTimes()) {
                gc1.setTime(d);
                if (gc.get(GregorianCalendar.DAY_OF_YEAR) == gc1.get(GregorianCalendar.DAY_OF_YEAR)
                        && gc1.get(GregorianCalendar.HOUR_OF_DAY) > gcSt.get(GregorianCalendar.HOUR_OF_DAY)
                        && gc1.get(GregorianCalendar.HOUR_OF_DAY) < gcFin.get(GregorianCalendar.HOUR_OF_DAY)) {
                    timeList.add(d);
                }
            }
        } else {
            log.error("Error: {}", gp.getSpError());
        }
    }

    @Command
    @NotifyChange({"done", "fail", "picked", "client", "inputDataRequired", "dateTimeRequired"})
    public void submit() {
        if (pickedRedirectServ.getInputRequired() && (inputData == null || inputData.isEmpty())) {
            inputDataRequiredText = l("input_data_required");
            return;
        }
        inputDataRequiredText = "";

        if (selectedDate == null || date == null) {
            dateTimeRequiredText = l("date_time_required");
            return;
        }
        dateTimeRequiredText = "";

        if (selectedDate != null && pickedRedirectServ != null) {
            log.debug("selectedDate={}", selectedDate);
            // ставим предварительного кастомера
            try {
                final QAdvanceCustomer result = netCommander.standInServiceAdvance(serviceList.getAppConfig().getNetProperty(), pickedRedirectServ.getId(), selectedDate, -1, getInputData(), "");
                client.setAdvClient(result);
                client.setInputData(inputData);
                client.setDate(selectedDate);
                Sessions.getCurrent().setAttribute("DATA", client);
                done = true;
            } catch (Exception ex) {
                log.error("[ERROR] stand In Servic Advance was fail. ", ex);
                fail = true;
                done = false;
            }
        }
    }

    @Command
    @NotifyChange("mailResult")
    public void sendMail() {
        if (client.getEmail() != null && !client.getEmail().isEmpty()) {
            try {
                mailer.sendReporterMail(client);
                mailResult = l("successfully");
            } catch (Exception ex) {
                mailResult = l("failure");
                log.error("[ERROR] Mail was not sent: ", ex);
            }
        }
    }
}
