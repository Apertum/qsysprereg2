package ru.apertum.qsystem.prereg;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ConfigurableApplicationContext;
import org.zkoss.bind.Property;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;
import ru.apertum.qsystem.prereg.core.Config;

import java.util.Map;

@Log4j2
public class ClientFormValidator extends AbstractValidator {

    public String l(String resName) {
        return Labels.getLabel(resName);
    }

    @Override
    public void validate(ValidationContext ctx) {
        if (1==1)return;
        final ConfigurableApplicationContext context = Application.context;
        final Config config = context.getBean(Config.class);

        Map<String, Property> beanProps = ctx.getProperties(ctx.getProperty().getBase());
        if (config.isFieldNameEnable()) {
            validateName(ctx, (String) beanProps.get("name").getValue());
            validateSourname(ctx, (String) beanProps.get("sourname").getValue());
        }
        if (config.isFieldEmailEnable()) {
            validateEmail(ctx, (String) beanProps.get("email").getValue());
        }
        if (config.isFieldPhoneEnable()) {
            validatePhone(ctx, ((String) beanProps.get("phone").getValue()).replaceAll("[^\\d.]", ""));
        }

        validateCaptcha(ctx, (String) ctx.getValidatorArg("captcha"), (String) ctx.getValidatorArg("captchaInput"));

        if (config.isField1Enable()) {
            validateField(ctx, "field1", ((String) beanProps.get("field1").getValue()), config.getField1Format(), config.getField1Hint());
        }
        if (config.isField2Enable()) {
            validateField(ctx, "field2", ((String) beanProps.get("field2").getValue()), config.getField2Format(), config.getField2Hint());
        }
        if (config.isField3Enable()) {
            validateField(ctx, "field3", ((String) beanProps.get("field3").getValue()), config.getField3Format(), config.getField3Hint());
        }
        if (config.isField4Enable()) {
            validateField(ctx, "field4", ((String) beanProps.get("field4").getValue()), config.getField4Format(), config.getField4Hint());
        }
        if (config.isField5Enable()) {
            validateField(ctx, "field5", ((String) beanProps.get("field5").getValue()), config.getField5Format(), config.getField5Hint());
        }
    }

    private void validateEmail(ValidationContext ctx, String email) {
        if (email != null && !email.isEmpty() && !email.matches(".+@.+\\.[a-z]{2,5}")) {
            this.addInvalidMessage(ctx, "email", l("bad_email"));
        }
    }

    private void validateCaptcha(ValidationContext ctx, String captcha, String captchaInput) {
        if (captchaInput == null || !captcha.equals(captchaInput)) {
            this.addInvalidMessage(ctx, "captcha", l("bad_captcha"));
        }
    }

    private void validateName(ValidationContext ctx, String string) {
        if (string == null || string.isEmpty() || string.length() < 2) {
            this.addInvalidMessage(ctx, "name", l("put_name"));
        }
    }

    private void validateSourname(ValidationContext ctx, String string) {
        if (string == null || string.isEmpty() || string.length() < 2) {
            this.addInvalidMessage(ctx, "sourname", l("put_surname"));
        }
    }

    private void validatePhone(ValidationContext ctx, String phone) {
        if (phone == null || phone.isEmpty() || !phone.matches("((7)|(8))\\d{10}")) {
            this.addInvalidMessage(ctx, "phone", "Укажите ваш мобильный номер");
        }
    }

    private void validateField(ValidationContext ctx, String key, String value, String format, String hint) {
        if (!value.matches(format)) {
            this.addInvalidMessage(ctx, key, hint);
        }
    }
}
