package ru.apertum.qsystem.prereg;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsystem.common.model.QAdvanceCustomer;
import ru.apertum.qsystem.prereg.core.AppConfig;
import ru.apertum.qsystem.prereg.core.Multilingual;
import ru.apertum.qsystem.server.model.QService;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static ru.apertum.qsystem.prereg.core.Multilingual.ARMENIAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.AZERBAIJAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.RUSSIAN_MONAT;
import static ru.apertum.qsystem.prereg.core.Multilingual.UKRAINIAN_MONAT;

@Component("client")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Data
@Log4j2
public class Client implements Serializable {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private Multilingual lng;

    /**
     * Формат даты.
     */
    public final DateFormat formatDdMm = new SimpleDateFormat("dd MMMM");
    public final DateFormat formatHhMm = new SimpleDateFormat("HH.mm");
    private String name;
    private String sourname;
    private String middlename;
    private String email;
    private String inputData;
    private QAdvanceCustomer advClient;
    private QService service;
    private Date date;
    private String day;
    private String finishT;

    private Boolean accepted = false;
    private String phone = "";

    private String field1 = "";
    private String field2 = "";
    private String field3 = "";
    private String field4 = "";
    private String field5 = "";


    /**
     * просят изменить отображения формата даты. сейчас пишет "Среда 29 января 16: 00" , а просят " 18.11.2019 15:51:00 " Стращают , говорят,  что сразу та просили )
     */
    public String getFormattedDate() {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(date);
    }

    public void setDate(Date date) {
        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols(lng.getCurrentLng().getLocale());
        switch (lng.getCurrentLng().getLocale().toString().toLowerCase(Locale.US)) {
            case "ru_ru":
                dateFormatSymbols.setMonths(RUSSIAN_MONAT);
                break;
            case "uk_ua":
                dateFormatSymbols.setMonths(UKRAINIAN_MONAT);
                break;
            case "az_az":
                dateFormatSymbols.setMonths(AZERBAIJAN_MONAT);
                break;
            case "hy_am":
                dateFormatSymbols.setMonths(ARMENIAN_MONAT);
                break;
            default:
                log.trace("No cpec monats.");
        }
        day = new SimpleDateFormat("EEEE dd MMMM", dateFormatSymbols).format(date);
        finishT = formatHhMm.format(date);
        this.date = date;
    }

    public void initClient(Client clientForm) {
        setName(clientForm.getName());
        setSourname(clientForm.getSourname());
        setMiddlename(clientForm.getMiddlename());
        setEmail(clientForm.getEmail());
        setPhone(clientForm.getPhone());
        setField1(clientForm.getField1());
        setField2(clientForm.getField2());
        setField3(clientForm.getField3());
        setField4(clientForm.getField4());
        setField5(clientForm.getField5());
    }

    public String getTitle() {
        return appConfig.getValue("QSYSPREREG_TITLE");
    }

    public String getCaption() {
        return appConfig.getValue("QSYSPREREG_CAPTION");
    }
}
