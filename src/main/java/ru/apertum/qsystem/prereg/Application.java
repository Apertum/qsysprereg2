package ru.apertum.qsystem.prereg;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@SpringBootApplication
@Log4j2
public class Application implements CommandLineRunner {

    public static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        final File log4j = new File("config/log4j2.xml");
        if (log4j.exists()) {
            Configurator.initialize("QSysPreReg2", log4j.getAbsolutePath());
        }
        try {
            prepareConfig();
        } catch (IOException ex) {
            log.error("Config prepare fail.", ex);
            throw new RuntimeException(ex);
        }
        context = SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("QSysPreReg2 was started successfully\nGO GO GO\nGO GO GO\nGO GO GO\nGO GO GO");
    }

    private static void prepareConfig() throws IOException {
        File dir = new File("config");
        if (!dir.exists() || !dir.isDirectory()) {
            log.info("Create config directory");
            Files.createDirectory(dir.toPath());
        }
        makeReady("config/qserver.properties");
        makeReady("config/config.properties");
        makeReady("config/email.html");
        makeReady("config/email.properties");
        makeReady("config/net.properties");
    }

    private static void makeReady(String fileName) throws IOException {
        File file = new File(fileName);
        if (!file.exists()) {
            log.info("Create config file '{}'", file.getName());
            try (final InputStream stream = Application.class.getResourceAsStream("/tmp/" + fileName)) {
                Files.copy(stream, file.toPath());
            }
        }
    }
}
