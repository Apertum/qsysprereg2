package ru.apertum.qsystem.prereg;

import lombok.NonNull;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;

public class ClientOneFormValidator extends AbstractValidator {

    public String l(String resName) {
        return Labels.getLabel(resName);
    }

    @Override
    public void validate(ValidationContext ctx) {
        validateCaptcha(ctx, (String) ctx.getValidatorArg("captcha"), (String) ctx.getValidatorArg("captchaInput"));
    }

    private void validateCaptcha(ValidationContext ctx, String captcha, @NonNull String captchaInput) {
        if (!captcha.equals(captchaInput)) {
            this.addInvalidMessage(ctx, "captcha", l("bad_captcha"));
        }
    }
}
