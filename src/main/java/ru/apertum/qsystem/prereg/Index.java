/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg;

import lombok.Getter;
import lombok.Setter;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import ru.apertum.qsystem.prereg.core.Multilingual;

/**
 * @author Evgeniy Egorov
 */
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class Index {

    @WireVariable("lng")
    private Multilingual lng;

    @Getter
    @Setter
    private Multilingual.Lng selectedLng;

    public String l(String resName) {
        return Labels.getLabel(resName);
    }

    @Command("changeLang")
    public void changeLang() {
        lng.setCurrentLng(selectedLng);
        lng.checkLocale(true);
    }

    @Init
    public void init() {
        selectedLng = lng.getCurrentLng();
        if (Executions.getCurrent().getParameter("onepage") != null) {
            Executions.sendRedirect("~./onepage.zul");
        }
    }
}
