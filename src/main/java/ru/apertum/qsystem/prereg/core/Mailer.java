/*
 * Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.prereg.core;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.prereg.Client;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Отослать почту.
 *
 * @author Evgeniy Egorov
 */
@SuppressWarnings("squid:S4797")
@Service("email")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Log4j2
public class Mailer {

    private static final MustacheFactory MUSTACHE_FACTORY = new DefaultMustacheFactory();

    @Autowired
    private EmailCnf emailCnf;

    /**
     * Отослать почту синхронно.
     */
    @SuppressWarnings({"squid:S3776", "squid:S4784"})
    public void sendReporterMail(Client client) {
        if (emailCnf.isEnable()) {
            try {
                Transport.send(prepareMessage(client));
            } catch (Exception ex) {
                throw new ServerException("Email was failed.", ex);
            }
        }
    }

    private MimeMessage prepareMessage(Client client) throws MessagingException, UnsupportedEncodingException {
        final Authenticator auth = new MyAuthenticator(emailCnf.getUser(), emailCnf.getPassword());
        final Properties properties;
        try {
            properties = emailCnf.getProperties();
        } catch (IOException e) {
            throw new ServerException("Error load email properties", e);
        }
        final Session session = Session.getDefaultInstance(properties, auth);
        final MimeMessage msg = new MimeMessage(session);

        msg.setSender(new InternetAddress(emailCnf.getFrom(), emailCnf.getFromTitle()));
        msg.setFrom(new InternetAddress(emailCnf.getFrom(), emailCnf.getFromTitle()));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(client.getEmail()));
        msg.setHeader("Content-Type", "text/html;charset=\"" + emailCnf.getCharset() + "\"");
        msg.setSubject(emailCnf.getSubject());

        final Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(getContent(emailCnf.getContent(), client));
        msg.setContent(multipart);

        return msg;
    }

    private BodyPart getContent(String content, Client client) throws MessagingException {
        // Все производим замены шаблонизатором.
        Mustache mustache;
        if (content != null && Paths.get(content).toFile().exists()) {
            mustache = MUSTACHE_FACTORY.compile(content);
        } else {
            try {
                mustache = MUSTACHE_FACTORY.compile("BOOT-INF/classes/email_def.html");
            } catch (Exception ex) {
                mustache = MUSTACHE_FACTORY.compile("email_def.html");
            }
        }
        final Map<String, Object> map = new HashMap<>();
        map.put("client", client);
        map.put("props", emailCnf);
        final String html = mustache.execute(new StringWriter(), map).toString();
        final BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(html, "text/html; charset=\"" + emailCnf.getCharset() + "\"");
        return messageBodyPart;
    }

    /**
     * Аутентификация. Логин пароль на почтовом серваке.
     */
    static class MyAuthenticator extends Authenticator {

        private final String user;
        private final String password;

        MyAuthenticator(String user, String password) {
            this.user = user;
            this.password = password;
        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(user, password);
        }
    }
}
