/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg.core;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * @author Evgeniy Egorov
 */
@Component("lng")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Log4j2
public class Multilingual {

    //*****************************************************
    //**** Multilingual
    //*****************************************************
    private static final Lng ENG_LNG = new Lng("English", "en-GB");

    private static final ArrayList<Lng> LANGS = new ArrayList<>(Arrays.asList(new Lng("Русский", "ru-RU"), ENG_LNG, new Lng("English", "en-US"), new Lng("Español", "es-ES"),
            new Lng("Deutsch", "de-DE"), new Lng("Português", "pt-PT"), new Lng("Français", "fr-FR"), new Lng("Italiano", "it-IT"), new Lng("čeština", "cs-CZ"),
            new Lng("Polski", "pl-PL"),
            new Lng("Slovenčina", "sk-SK"), new Lng("Român", "ro-RO"), new Lng("Cрпски", "sr-SP"), new Lng("Українська", "uk-UA"), new Lng("Türk", "tr-TR"),
            new Lng("हिंदी", "hi-IN"),
            new Lng("العربية", "ar-EG"), new Lng("עברית", "iw-IL"), new Lng("Қазақ", "kk-KZ"), new Lng("Indonesia", "in-ID"), new Lng("Suomi", "fi-FI")));

    public List<Lng> getLangs() {
        return LANGS;
    }

    public static final String[] RUSSIAN_MONAT = {
            "Января",
            "Февраля",
            "Марта",
            "Апреля",
            "Мая",
            "Июня",
            "Июля",
            "Августа",
            "Сентября",
            "Октября",
            "Ноября",
            "Декабря"
    };
    public static final String[] UKRAINIAN_MONAT = {
            "Січня",
            "Лютого",
            "Березня",
            "Квітня",
            "Травня",
            "Червня",
            "Липня",
            "Серпня",
            "Вересня",
            "Жовтня",
            "Листопада",
            "Грудня"
    };
    public static final String[] UKRAINIAN_MONAT_WEEKDAYS = {
            "субота",
            "неділя",
            "понеділок",
            "вівторок",
            "середа",
            "четвер",
            "п’ятниця",
            "субота",
            "неділя"
    };
    public static final String[] AZERBAIJAN_MONAT = {"Yanvar",
            "Fevral",
            "Mart",
            "Aprel",
            "May",
            "Iyun",
            "Iyul",
            "Avqust",
            "Sentyabr",
            "Oktyabr",
            "Noyabr",
            "Dekabr"};
    public static final String[] ARMENIAN_MONAT = {"Հունվարի",
            "Փետրվարի",
            "Մարտի",
            "Ապրիլի",
            "Մայիսի",
            "Հունիսի",
            "Հուլիսի",
            "Օգոստոսի",
            "Սեպտեմբերի",
            "Հոկտեմբերի",
            "Նոեմբերի",
            "Դեկտեմբերի"};

    @Getter
    @Setter
    private Lng currentLng = ENG_LNG;

    @Getter
    @Setter
    private boolean isFirst = true;

    /**
     * @param language типа мож дефолтный приехал
     * @param country  типа мож дефолтный приехал
     * @return code
     */
    public Lng init(String language, String country) {
        Lng lng = isFirst ? init(language + (country == null || country.isEmpty() ? "" : ("-" + country)), false) : currentLng;
        isFirst = false;
        return lng;
    }

    public Lng init(String lng, boolean redirectForce) {
        if (!isFirst) {
            return currentLng;
        }
        needRedirectForce = redirectForce;
        isFirst = false;

        log.info("Init Multilingual = {}", lng);

        Lng newLng = ENG_LNG;
        boolean f = true;
        for (Lng lang1 : LANGS) {
            if (lang1.tag.equalsIgnoreCase(lng)) {
                newLng = lang1;
                f = false;
                break;
            }
        }

        if (f) {
            for (Lng lang1 : LANGS) {
                if (lang1.tag.toLowerCase().startsWith(lng.split("-")[0].toLowerCase())) {
                    newLng = lang1;
                    break;
                }
            }
        }
        setCurrentLng(newLng);
        log.info("Inited: {}", newLng);
        return newLng;
    }

    private boolean needRedirectForce = false;

    public void checkLocale(boolean redirect) {
        final org.zkoss.zk.ui.Session sess = Sessions.getCurrent();
        if (sess == null) {
            return;
        }

        Locale sesLocale = sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE) == null ? Locale.ENGLISH
                : (Locale) sess.getAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE);

        if (!sesLocale.equals(currentLng.locale)) {
            sess.setAttribute(org.zkoss.web.Attributes.PREFERRED_LOCALE, currentLng.locale);
            if (needRedirectForce || redirect) {
                Executions.sendRedirect(null);
                needRedirectForce = false;
            }
        }
    }

    @Data
    public static class Lng {

        private final String name;
        private final String tag;
        private final String code;
        private final Locale locale;

        public Lng(String name, String tag) {
            this.name = name;
            this.tag = tag;
            this.code = tag.replace("-", "_");
            locale = Locale.forLanguageTag(tag.toLowerCase());
        }

        @Override
        public String toString() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof Lng && locale.equals(((Lng) o).locale);
        }

        @Override
        public int hashCode() {
            return locale.hashCode();
        }
    }
}
