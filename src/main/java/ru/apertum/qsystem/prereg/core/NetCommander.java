/*
 *  Copyright (C) 2010 {Apertum}Projects. web: www.apertum.ru email: info@apertum.ru
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qsystem.prereg.core;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import ru.apertum.qsystem.common.GsonPool;
import ru.apertum.qsystem.common.QException;
import ru.apertum.qsystem.common.Uses;
import ru.apertum.qsystem.common.cmd.AJsonRPC20;
import ru.apertum.qsystem.common.cmd.CmdParams;
import ru.apertum.qsystem.common.cmd.JsonRPC20;
import ru.apertum.qsystem.common.cmd.JsonRPC20Error;
import ru.apertum.qsystem.common.cmd.RpcGetAdvanceCustomer;
import ru.apertum.qsystem.common.cmd.RpcGetAllServices;
import ru.apertum.qsystem.common.cmd.RpcGetGridOfWeek;
import ru.apertum.qsystem.common.model.INetProperty;
import ru.apertum.qsystem.common.model.QAdvanceCustomer;
import ru.apertum.qsystem.server.model.QService;
import ru.apertum.qsystem.server.model.QServiceTree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

import static org.springframework.http.HttpHeaders.USER_AGENT;

/**
 * Содержит статические методы отправки и получения заданий на сервер.
 * любой метод возвращает XML-узел ответа сервера.
 *
 * @author Evgeniy Egorov
 */
@Service("net")
public class NetCommander {

    private static Logger log = LogManager.getLogger(NetCommander.class);

    private static final String CMD_URL_PATTERN = "/qsystem/command";

    private static final JsonRPC20 JSON_RPC = new JsonRPC20();

    @Autowired
    private NetCnf netCnf;

    /**
     * основная работа по отсылки и получению результата.
     *
     * @param netProperty сеть. параметры соединения с сервером
     * @param commandName имя команды.
     * @param params      параметры.
     * @return текст-ответ
     * @throws QException что-то пошло не так.
     */
    private synchronized String send(INetProperty netProperty, String commandName, CmdParams params) throws QException {
        JSON_RPC.setMethod(commandName);
        JSON_RPC.setParams(params);
        return sendRpc(netProperty, JSON_RPC);
    }

    private Proxy getProxy(Proxy.Type proxyType) {
        if (netCnf.isProxyEnable()) {
            log.trace("Proxy.Type." + proxyType + ": " + netCnf.getProxyHost() + ":" + netCnf.getProxyPort());
            return new Proxy(proxyType, new InetSocketAddress(netCnf.getProxyHost(), netCnf.getProxyPort()));
        } else {
            return null;
        }
    }

    /**
     * Отсылаем команду и получаем ответ. По TCP.
     *
     * @param netProperty сеть.
     * @param jsonRpc     команда.
     * @return текстовый ответ.
     * @throws QException упало.
     */
    @SuppressWarnings("squid:S3776")
    private synchronized String sendRpc(INetProperty netProperty, JsonRPC20 jsonRpc) throws QException {
        final String message;
        Gson gson = GsonPool.getInstance().borrowGson();
        try {
            message = gson.toJson(jsonRpc);
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
        final String data;

        if (netCnf.isUseHttp() && !(jsonRpc.getMethod().startsWith("#") || "empty".equalsIgnoreCase(jsonRpc.getMethod()))) {
            try {
                data = netCnf.isPost() ? sendPost(netProperty, message, jsonRpc) : sendGet(netProperty, jsonRpc);
            } catch (Exception ex) {
                throw new QException("no_response_from_server", ex);
            }
        } else {
            final Proxy proxy = getProxy(Proxy.Type.SOCKS);

            log.trace("Task \"" + jsonRpc.getMethod() + "\" on " + netProperty.getAddress().getHostAddress() + ":" + netProperty.getPort() + "#\n" + message);
            try (final Socket socket = proxy == null ? (!netCnf.isProxyEnable() ? new Socket(Proxy.NO_PROXY) : new Socket()) : new Socket(proxy)) {
                log.trace("Socket was created.");
                socket.connect(new InetSocketAddress(netProperty.getAddress(), netProperty.getPort()), 15000);

                final PrintWriter writer = new PrintWriter(socket.getOutputStream());
                writer.print(URLEncoder.encode(message, "utf-8"));
                log.trace("Sending...");
                writer.flush();
                log.trace("Reading...");
                final StringBuilder sb = new StringBuilder();
                final Scanner in = new Scanner(socket.getInputStream());
                while (in.hasNextLine()) {
                    sb.append(in.nextLine()).append("\n");
                }
                data = URLDecoder.decode(sb.toString(), "utf-8");
                sb.setLength(0);
                writer.close();
                in.close();
            } catch (IOException ex) {
                throw new QException("no_connect_to_server", ex);
            } catch (Exception ex) {
                throw new QException("no_response_from_server", ex);
            }
            log.trace("Response:\n{}", data);
        }

        gson = GsonPool.getInstance().borrowGson();
        try {
            final JsonRPC20Error rpc = gson.fromJson(data, JsonRPC20Error.class);
            if (rpc == null) {
                throw new QException("error_on_server_no_get_response");
            }
            if (rpc.getError() != null) {
                throw new QException("tack_failed" + " " + rpc.getError().getCode() + ":" + rpc.getError().getMessage());
            }
        } catch (JsonSyntaxException ex) {
            throw new QException("bad_response", ex);
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
        return data;
    }

    // HTTP POST request
    private String sendPost(INetProperty netProperty, String outputData, JsonRPC20 jsonRpc) throws Exception { // NOSONAR
        String url = "http://" + netProperty.getAddress().getHostAddress() + ":" + netProperty.getPort() + CMD_URL_PATTERN;
        log.debug("HTTP POST request \"{}\" on {}\n{}", jsonRpc.getMethod(), url, outputData);
        final URL obj = new URL(url);
        final Proxy proxy = getProxy(Proxy.Type.HTTP);

        final StringBuilder response;
        final HttpURLConnection con = (HttpURLConnection) (proxy == null
                ? (!netCnf.isProxyEnable() ? obj.openConnection(Proxy.NO_PROXY) : obj.openConnection())
                : obj.openConnection(proxy));
        try {
            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty(HttpHeaders.CONTENT_TYPE, "text/json; charset=UTF-8");

            // Send post request
            con.setDoOutput(true);

            try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), StandardCharsets.UTF_8))) {
                out.append(outputData);
                out.flush();
            }

            if (con.getResponseCode() != 200) {
                log.error("HTTP POST response code = {}", con.getResponseCode());
                throw new QException("no_connect_to_server");
            }

            response = readResponseFromStream(con.getInputStream());
        } finally {
            con.disconnect();
        }

        //result
        final String res = response.toString();
        response.setLength(0);
        log.trace("HTTP POST response:\n{}", res);
        return res;
    }

    private StringBuilder readResponseFromStream(InputStream inputStream) throws IOException {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
            String inputLine;
            final StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            return response;
        }
    }

    // HTTP GET request
    private String sendGet(INetProperty netProperty, JsonRPC20 jsonRpc) throws Exception { // NOSONAR
        final String p = jsonRpc.getParams() == null ? "" : jsonRpc.getParams().toString();
        final String url = "http://" + netProperty.getAddress().getHostAddress() + ":" + netProperty.getPort() + CMD_URL_PATTERN + "?"
                + CmdParams.CMD + "=" + URLEncoder.encode(jsonRpc.getMethod(), "utf-8") + "&"
                + p;
        log.debug("HTTP GET request \"{}\" on {}\n{}", jsonRpc.getMethod(), url, p);
        final URL obj = new URL(url);
        final Proxy proxy = getProxy(Proxy.Type.HTTP);

        final HttpURLConnection con = (HttpURLConnection) (proxy == null
                ? (!netCnf.isProxyEnable() ? obj.openConnection(Proxy.NO_PROXY) : obj.openConnection())
                : obj.openConnection(proxy));
        final StringBuilder response;
        try {
            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);

            if (con.getResponseCode() != 200) {
                log.error("HTTP GET response code = {}", con.getResponseCode());
                throw new QException("no_connect_to_server");
            }
            response = readResponseFromStream(con.getInputStream());
        } finally {
            con.disconnect();
        }

        //result
        final String res = response.toString();
        response.setLength(0);
        log.trace("HTTP GET response:\n{}", res);
        return res;
    }

    /**
     * Получение возможных услуг.
     *
     * @param netProperty сеть. параметры соединения с сервером
     * @return XML-ответ
     */
    public RpcGetAllServices.ServicesForWelcome getServices(INetProperty netProperty) throws QException {
        log.info("Получение возможных услуг.");
        // загрузим ответ
        String res = send(netProperty, Uses.TASK_GET_SERVICES, null);
        final RpcGetAllServices allServices = AJsonRPC20.demarshal(res, RpcGetAllServices.class);
        QServiceTree.sailToStorm(allServices.getResult().getRoot(), service -> {
            final QService perent = (QService) service;
            perent.getChildren().forEach(svr -> svr.setParent(perent));
        });
        return allServices.getResult();
    }

    /**
     * Получение недельной таблици с данными для предварительной записи.
     *
     * @param netProperty      сеть.      netProperty параметры соединения с сервером.
     * @param serviceId        услуга, в которую пытаемся встать.
     * @param date             первый день недели за которую нужны данные.
     * @param advancedCustomer ID авторизованного кастомера
     * @return класс с параметрами и списком времен
     */
    public RpcGetGridOfWeek.GridAndParams getGridOfWeek(INetProperty netProperty, long serviceId, Date date, long advancedCustomer) throws QException {
        log.info("Получить таблицу");
        // загрузим ответ
        final CmdParams params = new CmdParams();
        params.setServiceId(serviceId);
        params.setDate(date.getTime());
        params.setCustomerId(advancedCustomer);
        final String res = send(netProperty, Uses.TASK_GET_GRID_OF_WEEK, params);
        return AJsonRPC20.demarshal(res, RpcGetGridOfWeek.class).getResult();
    }

    /**
     * Предварительная запись в очередь.
     *
     * @param netProperty      сеть.      netProperty параметры соединения с сервером.
     * @param serviceId        услуга, в которую пытаемся встать.
     * @param date             Дата записи
     * @param advancedCustomer ID авторизованного кастомер. -1 если нет авторизации
     * @param inputData        введеные по требованию услуги данные клиентом, может быть null если не вводили
     * @param comments         комментарий по предварительно ставящемуся клиенту если ставят из админки или приемной
     * @return предварительный кастомер
     */
    public QAdvanceCustomer standInServiceAdvance(INetProperty netProperty, long serviceId, Date date, long advancedCustomer, String inputData, String comments)
            throws QException {
        log.info("Записать предварительно в очередь.");
        // загрузим ответ
        final CmdParams params = new CmdParams();
        params.setServiceId(serviceId);
        params.setDate(date.getTime());
        params.setCustomerId(advancedCustomer);
        params.setTextData(inputData);
        params.setComments(comments);
        final String res = send(netProperty, Uses.TASK_ADVANCE_STAND_IN, params);
        return AJsonRPC20.demarshal(res, RpcGetAdvanceCustomer.class).getResult();
    }
}
