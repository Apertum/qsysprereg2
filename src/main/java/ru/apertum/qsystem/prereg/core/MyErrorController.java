package ru.apertum.qsystem.prereg.core;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class MyErrorController implements ErrorController {

    @GetMapping(path = "/error")
    public String handleError(HttpServletRequest request, HttpServletResponse httpResponse) throws IOException {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "<html>error-404<br>Page not found.<br>Lets go to <a href='/'>home</a>";
            } else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                httpResponse.sendRedirect("error.zul");
                return null;
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}

