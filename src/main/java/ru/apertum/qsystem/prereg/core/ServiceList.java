/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg.core;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.server.model.QService;

import javax.annotation.PostConstruct;
import javax.swing.tree.TreeNode;
import java.util.LinkedList;

import static ru.apertum.qsystem.server.model.ATreeModel.sailToStorm;

/**
 * @author Evgeniy Egorov
 */
@Component("services")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ServiceList {

    @Autowired
    private NetCommander netCommander;

    @Autowired
    @Getter
    private AppConfig appConfig;

    @Getter
    private QService root;

    @Getter
    private LinkedList<QService> list = new LinkedList<>();

    @PostConstruct
    public void initDevelopmentProperties() {
        try {
            root = netCommander.getServices(appConfig.getNetProperty()).getRoot();
        } catch (Exception ex) {
            throw new ServerException("Bad net conversation: " + ex);
        }

        list.clear();
        sailToStorm(root, (TreeNode service) -> {
            if (service.isLeaf()
                    && ((QService) service).getAdvanceLimit() != 0
                    && QService.STATUS_FOR_USING.contains(((QService) service).getStatus())
                    && ((QService) service).getStatus() != 4) {
                list.add((QService) service);
            }
        });
    }


}
