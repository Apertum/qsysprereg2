package ru.apertum.qsystem.prereg.core;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

/**
 * Сожержание.
 * net.proxy=false
 * net.proxy.host=127.0.0.1
 * net.proxy.port=27028
 * <p>
 * net.http.enable=false
 * net.http.protocol=post
 */
@Configuration("netCnf")
@PropertySource(value = "file:config/net.properties", name = "net", encoding = "utf8")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Data
public class NetCnf {

    @Value("${net.proxy}")
    private boolean proxyEnable;
    @Value("${net.proxy.port}")
    private int proxyPort;
    @Value("${net.proxy.host}")
    private String proxyHost;
    @Value("${net.http.enable}")
    private boolean useHttp;
    @Value("${net.http.protocol}")
    private String httpProtocol;

    public boolean isPost() {
        return "post".equalsIgnoreCase(httpProtocol);
    }
}
