package ru.apertum.qsystem.prereg.core;

import lombok.Data;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * mail.enable=false
 * mail.smtp.port=587
 * mail.smtp.host=smtp.gmail.com
 * mail.smtp.auth=true
 * mail.smtp.user=my.company
 * mail.smtp.password=mypass
 * mail.smtp.from=my.company@gmail.com
 * mail.smtp.fromTitle=QSystem
 * mail.smtp.starttls=true
 * mail.mime.charset=UTF-8
 * mail.subject=QSystem. Pre-regidtration.
 * <p>
 * # файл содержания письма или строка содержания письма / text of message or file
 * mail.content=content.html
 */
@Configuration("emailCnf")
@PropertySource(value = "file:config/email.properties", name = "email", encoding = "utf8")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Data
public class EmailCnf {

    private static Logger log = LogManager.getLogger(EmailCnf.class);

    @Value("${mail.enable}")
    private boolean enable;
    @Value("${mail.smtp.port}")
    private int port;
    @Value("${mail.smtp.host}")
    private String host;
    @Value("${mail.smtp.auth}")
    private boolean auth;
    @Value("${mail.smtp.user}")
    private String user;
    @Value("${mail.smtp.password}")
    private String password;
    @Value("${mail.smtp.from}")
    private String from;
    @Value("${mail.smtp.fromTitle}")
    private String fromTitle;
    @Value("${mail.smtp.starttls.enable}")
    private boolean starttls;
    @Value("${mail.mime.charset}")
    private String charset;
    @Value("${mail.subject}")
    private String subject;
    @Value("${mail.content}")
    private String content;

    public Properties getProperties() throws IOException {
        final Properties properties = new Properties();
        properties.load(new InputStreamReader(new FileInputStream("config/email.properties"), StandardCharsets.UTF_8));
        return properties;
    }

    public String getFromTitle() {
        return fromTitle == null || fromTitle.isEmpty() ? "QSystem pre-registration email." : fromTitle;
    }

    public String getCharset() {
        return charset == null || charset.isEmpty() ? "UTF-8" : charset;
    }

    public String getSubject() {
        return subject == null || subject.isEmpty() ? "QSystem pre-registration email." : subject;
    }
}
