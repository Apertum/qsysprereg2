package ru.apertum.qsystem.prereg.core;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.context.WebApplicationContext;
import org.zkoss.lang.Library;
import org.zkoss.zk.ui.WebApps;
import ru.apertum.qsystem.common.ServerException;
import ru.apertum.qsystem.common.model.INetProperty;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Configuration("cnf")
@PropertySource(value = "file:config/qserver.properties", name = "qserver", encoding = "utf8")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Log4j2
public class AppConfig {

    @Autowired
    ConfigurableEnvironment env;

    @Getter
    private String com = "";

    @Resource(name = "cnfPropertyBean")
    @Getter
    @Setter
    private Map<String, String> map;
    /**
     * Пример получения ресурсов из окружения.
     */
    private Map<String, String> properties;

    /**
     * Как использовать в бинах спринга:
     *
     * @return возвращает практически мапу, хотя и некая фабрика.
     * @ Resource ( name = "cnfPropertyBean")
     * private Map<String, String> configMap;
     */
    @Bean(name = "cnfPropertyBean")
    public static PropertiesFactoryBean mapper() {
        final PropertiesFactoryBean bean = new PropertiesFactoryBean();
        bean.setFileEncoding(StandardCharsets.UTF_8.name());
        bean.setLocation(new FileSystemResource("config/qserver.properties"));
        return bean;
    }

    public void setCom(String com) {
        this.com = com == null || com.isEmpty() ? "" : com;
        log.trace("setCom com='{}'", this.com);
    }

    public String getValue(String name) {
        return map.get(com.isEmpty() ? name : com + "_" + name);
    }

    public INetProperty getNetProperty() {
        return getNetProperty(com);
    }

    public INetProperty getNetProperty(String pref) {
        if (pref == null || pref.isEmpty()) {
            pref = "";
            com = pref;
        } else {
            com = pref;
            pref = pref + "_";
        }
        log.debug("getNetProperty pref='{}'", pref);


        final int port = Integer.parseInt(map.get(pref + "QSYSTEM_SERVER_PORT") == null ? "3128" : map.get(pref + "QSYSTEM_SERVER_PORT"));
        final String host = map.get(pref + "QSYSTEM_SERVER_ADDR") == null ? "127.0.0.1" : map.get(pref + "QSYSTEM_SERVER_ADDR");
        log.debug("QSystem[{}] will be found at {}:{}", pref, host, port);

        return new INetProperty() {
            @Override
            public Integer getPort() {
                return port;
            }

            @Override
            public InetAddress getAddress() {
                try {
                    return InetAddress.getByName(host);
                } catch (UnknownHostException ex) {
                    throw new ServerException("Wrong address of server: " + host);
                }
            }
        };

    }

    public Map<String, String> getProperties() {
        properties = (Map<String, String>) env.getPropertySources().get("qserver").getSource();
        return properties;
    }

    @PostConstruct
    public void initDevelopmentProperties() {
        getProperties(); // <<-- вызвать тут для иницыализации. Это в рамках примера.
        log.info("**************************************************************");
        log.info("****      ZK-Springboot-QS: app configuration active      ****");
        log.info("**************************************************************");
        //disable various caches to avoid server restarts
        Library.setProperty("org.zkoss.zk.ZUML.cache", "false");
        Library.setProperty("org.zkoss.zk.WPD.cache", "false");
        Library.setProperty("org.zkoss.zk.WCS.cache", "false");
        Library.setProperty("org.zkoss.web.classWebResource.cache", "false");
        Library.setProperty("org.zkoss.util.label.cache", "false");
        // enable non minified js
        WebApps.getCurrent().getConfiguration().setDebugJS(true);
        // enable for debugging MVVM commands and binding (very verbose)
        Library.setProperty("org.zkoss.bind.DebuggerFactory.enable", "false");
    }
}

