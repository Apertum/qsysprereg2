/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qsystem.prereg.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.apertum.qsystem.prereg.Version;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Evgeniy Egorov
 */
@Component
@Order(1)
public class ZulFilter implements Filter {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private Multilingual lng;

    private FilterConfig filterConfig;

    @Bean
    public FilterRegistrationBean regFilter() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(this);
        registrationBean.addUrlPatterns("/");
        registrationBean.addUrlPatterns("/zkau/web/index.zul");
        registrationBean.addUrlPatterns("/zkau/web/onepage.zul");
        return registrationBean;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpRequest.getParameterMap().forEach((k, v) -> {
            if ("com".equals(k)) {
                appConfig.setCom(v[0]);
            }
            if ("locale".equals(k)) {
                lng.init(v[0], true);
            }
        });
        lng.init(request.getLocale().getLanguage(), request.getLocale().getCountry());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        //noting
    }

    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        Version.print();
    }
}
