package ru.apertum.qsystem.prereg.core;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

@Configuration("config")
@PropertySource(value = "file:config/config.properties", name = "config", encoding = "utf8")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Log4j2
@Data
public class Config {

    @Value("${consent_detailed_link}")
    private String consentDetailedLink;

    @Value("${screen.width}")
    private String screenWidth;

    @Value("${screen.height}")
    private String screenHeight;

    @Value("${app.useLanguages}")
    private boolean useLanguages = false;


    @Value("${screen.index.grid.width}")
    private String screenIndexGridWidth;

    @Value("${screen.index.column1.width}")
    private String screenIndexColumn1Width;

    @Value("${screen.index.column2.width}")
    private String screenIndexColumn2Width;

    @Value("${screen.index.textbox.width}")
    private String screenIndexTextboxWidth;

    @Value("${screen.selectService.listbox.pageSize}")
    private int screenSelectServiceListboxPageSize;

    @Value("${screen.selectService.listbox.height}")
    private String screenSelectServiceListboxHeight;

    @Value("${screen.finish.vbox.width}")
    private String screenFinishVboxWidth;

    //=======================================================================
    //=======================================================================
    //=======================================================================

    @Value("${field.name.enable}")
    private boolean fieldNameEnable;

    @Value("${field.name.asInputed}")
    private boolean fieldNameAsInputed;

    @Value("${field.email.enable}")
    private boolean fieldEmailEnable;

    @Value("${field.email.asInputed}")
    private boolean fieldEmailAsInputed;

    @Value("${field.phone.enable}")
    private boolean fieldPhoneEnable;

    @Value("${field.phone.asInputed}")
    private boolean fieldPhoneAsInputed;

    @Value("${field1.enable}")
    private boolean field1Enable;

    @Value("${field1.caption}")
    private String field1Caption;

    @Value("${field1.format}")
    private String field1Format;

    @Value("${field1.hint}")
    private String field1Hint;

    @Value("${field1.asInputed}")
    private boolean field1AsInputed;

    @Value("${field2.enable}")
    private boolean field2Enable;

    @Value("${field2.caption}")
    private String field2Caption;

    @Value("${field2.format}")
    private String field2Format;

    @Value("${field2.hint}")
    private String field2Hint;

    @Value("${field2.asInputed}")
    private boolean field2AsInputed;

    @Value("${field3.enable}")
    private boolean field3Enable;

    @Value("${field3.caption}")
    private String field3Caption;

    @Value("${field3.format}")
    private String field3Format;

    @Value("${field3.hint}")
    private String field3Hint;

    @Value("${field3.asInputed}")
    private boolean field3AsInputed;

    @Value("${field4.enable}")
    private boolean field4Enable;

    @Value("${field4.caption}")
    private String field4Caption;

    @Value("${field4.format}")
    private String field4Format;

    @Value("${field4.hint}")
    private String field4Hint;

    @Value("${field4.asInputed}")
    private boolean field4AsInputed;

    @Value("${field5.enable}")
    private boolean field5Enable;

    @Value("${field5.caption}")
    private String field5Caption;

    @Value("${field5.format}")
    private String field5Format;

    @Value("${field5.hint}")
    private String field5Hint;

    @Value("${field5.asInputed}")
    private boolean field5AsInputed;

    public boolean needConsent() {
        return consentDetailedLink != null && !consentDetailedLink.isEmpty();
    }
}

